const express = require('express');
const app = express();
const bodyParser = require('body-parser').json();
const fileStorage = require('fs');

require('./app/routes')(app, bodyParser, fileStorage);

app.use(express.static('public'));
app.listen(80);
console.log('Server starter at http://localhost:80');