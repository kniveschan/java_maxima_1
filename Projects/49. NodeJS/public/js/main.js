function sendUser(firstName, lastName) {
    let body = {
        "firstName": firstName,
        "lastName": lastName
    };

    $.ajax({
        type: "POST",
        url: "/users",
        data: JSON.stringify(body),
        dataType: "json",
        contentType: "application/json"
    });
}