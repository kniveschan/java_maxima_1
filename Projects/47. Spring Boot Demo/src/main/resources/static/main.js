function addCar(color, model, csrfTokenValue) {
    let body = {
        "color": color,
        "model": model
    };

    $.ajax({
        type: "POST",
        url: "/cars",
        headers: {
            'X-CSRF-TOKEN': csrfTokenValue
        },
        data: JSON.stringify(body),
        success: response => renderCarTable(response),
        error: function () {
            alert("Ошибка")
        },
        dataType: "json",
        contentType: "application/json"
    });
}

function renderCarTable(response) {
    let html = '<tr>' +
        '<th>Color</th>' +
        '<th>Model</th>' +
        '</tr>';

    for (let i = 0; i < response.length; i++) {
        html += '<tr>';
        html += '<td>' + response[i]['color'] + '</td>';
        html += '<td>' + response[i]['model'] + '</td>';
        html += '</tr>'
    }

    $('#car_table').empty().append(html);
}