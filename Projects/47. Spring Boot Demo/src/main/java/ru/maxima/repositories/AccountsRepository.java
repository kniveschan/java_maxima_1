package ru.maxima.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.maxima.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 18.10.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface AccountsRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByEmail(String email);

    List<Account> findAllByState(Account.State state);

    List<Account> findByEmailLike(String email);

    Optional<Account> findByConfirmUUID(String uuid);
}
