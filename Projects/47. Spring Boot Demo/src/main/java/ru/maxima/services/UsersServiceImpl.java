package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.dto.CarDto;
import ru.maxima.exceptions.UserNotFoundException;
import ru.maxima.models.Account;
import ru.maxima.models.Car;
import ru.maxima.repositories.AccountsRepository;
import ru.maxima.repositories.CarsRepository;

import java.util.List;

import static ru.maxima.dto.CarDto.from;

/**
 * 06.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;

    private final CarsRepository carsRepository;

    @Override
    public List<AccountDto> getAllUsers() {
        return AccountDto.from(accountsRepository.findAllByState(Account.State.CONFIRMED));
    }

    @Override
    public void deleteUser(Long userId) {
        Account account = accountsRepository.getById(userId);
        account.setState(Account.State.DELETED);
        accountsRepository.save(account);
    }

    @Override
    public List<CarDto> getCarsByUser(Long userId) {
        accountsRepository.findById(userId).orElseThrow(UserNotFoundException::new);
        return from(carsRepository.findAllByAccount_Id(userId));
    }

    @Override
    public void addCarToUser(Long userId, CarDto carForm) {
        Account account = accountsRepository.getById(userId);
        Car car = carsRepository.getById(carForm.getId());
        car.setAccount(account);
        carsRepository.save(car);
    }

    @Override
    public List<CarDto> getCarsWithoutUser() {
        return from(carsRepository.findAllByAccountIsNull());
    }
}
