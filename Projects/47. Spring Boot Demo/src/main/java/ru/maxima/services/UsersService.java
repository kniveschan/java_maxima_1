package ru.maxima.services;

import ru.maxima.dto.AccountDto;
import ru.maxima.dto.CarDto;

import java.util.List;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersService {
    List<AccountDto> getAllUsers();

    void deleteUser(Long userId);

    List<CarDto> getCarsByUser(Long userId);

    void addCarToUser(Long userId, CarDto car);

    List<CarDto> getCarsWithoutUser();
}
