package ru.maxima.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.maxima.dto.ResponseWithTimeDto;

import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * 06.12.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Slf4j
@Component
@Aspect
public class TimeAspect {
    @Before(value = "execution(String ru.maxima.controllers.CarsController.getCarsPage(..))")
    public void beforeControllerMethodExecution() {
        log.info("Current time - {}", LocalTime.now().toString());
    }

    @AfterReturning(value = "execution(String ru.maxima.controllers.*.*(..))", returning = "pageName")
    public void afterControllerMethodExecution(String pageName) {
        log.info("Current time - {} for page - {}", LocalTime.now().toString(), pageName);
    }

    @Around(value = "execution(* ru.maxima.controllers.SearchController.search*(..))")
    public ResponseEntity<?> addTimeInformationToResponse(ProceedingJoinPoint joinPoint) throws Throwable {
        long before = System.currentTimeMillis();
        Object response = joinPoint.proceed();
        long after = System.currentTimeMillis();
        return ResponseEntity.ok(ResponseWithTimeDto.builder()
                .data(((ResponseEntity)response).getBody())
                .mills(after - before)
                .build());
    }

}
