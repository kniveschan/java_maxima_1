package ru.maxima.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.maxima.dto.CarDto;
import ru.maxima.services.CarsService;

import java.util.List;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
@RequiredArgsConstructor
@RequestMapping("/cars")
public class CarsController {

    private final CarsService carsService;

    @GetMapping
    public String getCarsPage(@AuthenticationPrincipal(expression = "account.isAdmin()") Boolean isAdmin, Model model) {
        model.addAttribute("isAdmin", isAdmin);
        model.addAttribute("cars", carsService.getAllCars());
        return "cars";
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public List<CarDto> addCar(@RequestBody CarDto car) {
        carsService.addCar(car);
        return carsService.getAllCars();
    }
}
