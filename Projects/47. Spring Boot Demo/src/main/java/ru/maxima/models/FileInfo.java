package ru.maxima.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 15.11.2021
 * Spring Boot Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
public class FileInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // название файла на диске (генерируется случайным образом)
    private String storageFileName;
    // оригинальное название файла
    private String originalFileName;
    // тип файла (image/jpg, audio.mp3)
    private String mimeType;
    // размера загружаемого файла
    private Long size;
    // описание
    private String description;
}
