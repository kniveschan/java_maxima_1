# Проблемы с работой базы данных и их решение

* Ваше приложение (будучи сложным клиент-серверным приложением, например, порталом банковского сервиса и т.д.)
может генерировать очень большое количество запросов в базу данных.

* Единственный объект, который может обеспечить взаимодействие между базой и приложением в рамках
JDBC - это Connection. Connection - некоторый объект, который связан с "физическим подключением" к базе данных
и предоставляющий данные из бд.

* Если запросов действительно очень много и они все отправляются в рамках одного Connection, то эти запросы
встают в очередь друг за другом и следующий запрос не выполнится, пока не закончится обработка предыдущего.

- Connection переполняется и начинает сильно тормозить.

## Решение первое

1. Использовать интерфейс DataSource для того, чтобы репозиторий не думал о том, откуда брать Connection.
2. Реализовали DataSource через класс `OncePerQueryDataSource`. Данный класс предоставляет новый объект `Connection`
и, следовательно, новое подключение для бд при каждом обращении (при каждом запросе).

Проблема этого решения:
База не выдерживает бесконечного (даже 100) количества подключений и начинает зависать.

## Решение второе

1. Использовали паттерн "ConnectionPool" и его конкретную реализацию `HikariCP`.
Суть этого подхода заключается в том, что создается некоторое относительно небольшое количество соединений,
например 20 для запросов. Новый запрос попадает в первый свободный `Connection` и выполняется. Остальные запросы
ждут своей очереди. Баланс между подключениями и запросы.

2. Есть один нюанс. Есть `Connection` как объект, и есть `Connection` как физическое подключение.
Для того, чтобы `HikariCP` работал корректно, количество объектов `Connection` должно быть ограничено.
Мы это можем реализовать с помощью "закрытия" объекта `Connection` после выполнения запроса.
Закрытие объекта даст `HikariCP` понять, что подключение "свободно". Поэтому мы используем
`connection.close()`. Но прежде, чем закрыть само подключение, необходимо закрыть объекты, которые
используют это подключения для гарантии корректной работы системы. Закрывать необходимо в порядке, обратном 
открытию.