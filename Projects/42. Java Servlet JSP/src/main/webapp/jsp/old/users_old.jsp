<%@ page import="java.util.List" %>
<%@ page import="ru.maxima.dto.AccountDto" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>List of users - <%=((List<AccountDto>) request.getAttribute("accounts")).size()%>
</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Email</th>
    </tr>
    <%
        List<AccountDto> accounts = (List<AccountDto>) request.getAttribute("accounts");
        for (AccountDto account : accounts) {
    %>
    <tr>
        <td><%=account.getId()%>
        </td>
        <td><%=account.getEmail()%>
        </td>
    </tr>
    <%
        }
    %>
</table>
</body>
</html>
