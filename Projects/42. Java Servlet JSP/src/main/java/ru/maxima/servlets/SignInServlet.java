package ru.maxima.servlets;

import org.springframework.context.ApplicationContext;
import ru.maxima.exeptions.IncorrectEmailOrPasswordException;
import ru.maxima.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 30.09.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {

    private UsersService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.usersService = applicationContext.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/signIn.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // берем из входящего запроса email и пароль
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        // проверяем, существует ли такой пользователь
        try {
            // если бы пользователя не было - выбросили бы исключение и оказались в блоке catch
            usersService.signIn(email, password);
            // создаем новую сессию, потому что все хорошо
            HttpSession session = request.getSession(true);
            // в атрибуты сессий кладем атрибут "isAuthenticated" со значением true
            session.setAttribute("isAuthenticated", true);
            // перенаправляем на profile
            response.sendRedirect("/profile");
        } catch (IncorrectEmailOrPasswordException e) {
            response.sendRedirect("/signIn");
        }
    }
}
