package ru.maxima.repositories.jpa;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.maxima.models.Account;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * 14.10.2021
 * 42. Java Servlet JSP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class AccountsRepositoryJpaImpl implements AccountsRepository {

//    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    @Override
    public void save(Account account) {
        entityManager.persist(account);
    }

    @Override
    public List<Account> findAll() {
        TypedQuery<Account> query = entityManager.createQuery("select account from Account account", Account.class);
        return query.getResultList();
    }

    @Override
    public Optional<Account> findByEmail(String email) {
        TypedQuery<Account> query = entityManager.createQuery("select account from Account account where account.email = :email",
                Account.class);
        query.setParameter("email", email);
        List<Account> result = query.getResultList();
        if (result.size() != 1) {
            return Optional.empty();
        }
        return Optional.of(result.get(0));
    }

    @Override
    public List<Account> searchByEmail(String email) {
        TypedQuery<Account> query = entityManager.createQuery("select account from Account account where account.email like :email",
                Account.class);
        query.setParameter("email", "%" + email.toLowerCase() + "%");
        return query.getResultList();
    }
}
