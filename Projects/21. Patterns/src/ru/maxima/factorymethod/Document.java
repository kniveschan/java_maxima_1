package ru.maxima.factorymethod;

/**
 * 06.07.2021
 * 21. Patterns
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Document {
    String getText();
    String getTitle();
}
