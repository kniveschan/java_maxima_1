import org.springframework.aop.framework.ProxyFactory;
import org.springframework.cglib.core.Local;

import java.time.LocalDateTime;

/**
 * 04.12.2021
 * 48. AOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        FileAccessor target = new FileAccessor();

        ProxyFactory proxyFactory = new ProxyFactory(target);
        proxyFactory.addAdvice(new AfterOpenFile());
        proxyFactory.addAdvice(new BeforeOpenFile());
        proxyFactory.addAdvice(new ThrowsOpenFile());

        FileAccessor proxy = (FileAccessor) proxyFactory.getProxy();
        proxy.openFile("input2.txt");


    }
}
