package ru.maxima.blacklist;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * 06.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Component
public class PasswordBlackListFileImpl implements PasswordBlackList {

    private final String fileName;

    public PasswordBlackListFileImpl(@Value("${passwordBlackListFile.fileName}") String fileName) {
        this.fileName = fileName;
    }

    public boolean contains(String password) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            if(reader.lines().anyMatch(line -> line.equals(password))) {
                System.err.println("Пароль найден в черном списке!");
                return true;
            } else return false;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
