package ru.maxima.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.maxima.dto.CarDto;
import ru.maxima.services.UsersService;

/**
 * 21.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class UsersController {

    @Autowired
    private UsersService usersService;

    @RequestMapping("/users")
    public String getUsers(Model model) {
       model.addAttribute("accounts", usersService.getAllUsers());
       return "users";
    }

    @RequestMapping("/users/{user-id}/cars")
    public String getCarsOfUserPage(@PathVariable("user-id") Long userId, Model model) {
        model.addAttribute("cars", usersService.getCarsByUser(userId));
        model.addAttribute("carsWithoutUser", usersService.getCarsWithoutUser());
        return "cars_of_user";
    }

    @RequestMapping(value = "/users/{user-id}/cars", method = RequestMethod.POST)
    public String addCarToUser(@PathVariable("user-id") Long userId, CarDto car) {
        usersService.addCarToUser(userId, car);
        return "redirect:/users/" + userId + "/cars";
    }
}
