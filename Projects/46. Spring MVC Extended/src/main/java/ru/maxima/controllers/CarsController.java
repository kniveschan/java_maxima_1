package ru.maxima.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.maxima.dto.CarDto;
import ru.maxima.services.CarsService;

import java.util.List;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Controller
public class CarsController {

    @Autowired
    private CarsService carsService;

    @RequestMapping("/cars")
    public String getCarsPage() {
        return "cars";
    }

    @RequestMapping(value = "/cars", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public List<CarDto> addCar(@RequestBody CarDto car) {
        carsService.addCar(car);
        return carsService.getAllCars();
    }
}
