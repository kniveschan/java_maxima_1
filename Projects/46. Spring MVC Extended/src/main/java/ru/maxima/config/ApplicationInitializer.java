package ru.maxima.config;

import lombok.SneakyThrows;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ApplicationInitializer implements WebApplicationInitializer {
    @Override
    @SneakyThrows
    public void onStartup(ServletContext servletContext) throws ServletException {
        PropertySource propertySource = new ResourcePropertySource("classpath:application.properties");


        AnnotationConfigWebApplicationContext springWebContext = new AnnotationConfigWebApplicationContext();

        String profile = (String) propertySource.getProperty("application.profile");
        springWebContext.getEnvironment().setActiveProfiles(profile);
        springWebContext.register(ApplicationConfig.class);

        ContextLoaderListener springContextLoaderListener = new ContextLoaderListener(springWebContext);

        servletContext.addListener(springContextLoaderListener);

        ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("dispatcher",
                new DispatcherServlet(springWebContext));
        dispatcherServlet.setLoadOnStartup(1);
        dispatcherServlet.addMapping("/");
    }
}
