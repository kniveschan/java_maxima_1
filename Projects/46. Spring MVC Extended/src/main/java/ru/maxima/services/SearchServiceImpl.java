package ru.maxima.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.repositories.AccountsRepository;

import java.util.List;

import static ru.maxima.dto.AccountDto.from;

/**
 * 27.10.2021
 * 46. Spring MVC Extended
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final AccountsRepository accountsRepository;

    @Override
    public List<AccountDto> searchUsersByEmail(String email) {
        return from(accountsRepository.findByEmailLike("%" + email + "%"));
    }
}
