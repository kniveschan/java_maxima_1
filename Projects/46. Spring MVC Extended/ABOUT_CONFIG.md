# Как устроено наше MVC-приложение?

## Часть 1

* Наше приложение собирается в WAR-архив.
* Потом это приложение деплоится в Tomcat, который является сторонним приложением.

## Часть 2

* В пакете config у нас есть класс `ApplicationInitializer`. Его главная задача - при запуске всего приложения
подтянуть ваш контекст Spring (`ApplicationConfig`), создать `DispatcherServlet` и связать их между собой через `ServletContext`.

## Часть 3

* В классе ApplicationConfig мы прописываем все нужные настройки и бины нашего приложения:

- `@PropertySource` - обозначаем путь к properties-файлу, откуда можно тянуть настройки приложения.
- `@EnableTransactionManagement` - включаем менеджер транзакции Spring
- `@EnableJpaRepositories` - включаем поддержку репозиториев Spring Data Jpa.
- `@EnableWebMvc` - включаем корректную работу mvc-фреймворка.

- `dataSource()` - бин на источник данных БД.
- `entityManagerFactory()` - фабрика для ORM-фреймворка. Позволяет соединять логику Hibernate и Spring
- `transactionManager()` - менеджер транзакций.
- `hikariConfig()` - конфигурация ConnectionPool
- `viewResolver()` - нужна для того, чтобы по названию view определять файл, в котором эта View есть.
- `freeMarkerConfig` - конфигурация Freemarker.

