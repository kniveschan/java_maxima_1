package ru.maxima.spring.repositories;

import ru.maxima.spring.models.Course;

import java.util.List;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface CoursesRepository {
    void save(Course course);
    List<Course> findAllByLesson_name(String name);
}
