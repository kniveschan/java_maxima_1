package ru.maxima.jpa.repositories;

import ru.maxima.jpa.models.Course;
import ru.maxima.jpa.models.Lesson;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface LessonsRepository {
    void save(Lesson lesson);
}
