package ru.maxima.jpa.repositories;

import ru.maxima.jpa.models.Student;

import java.util.List;

/**
 * 13.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface StudentsRepository {
    void save(Student student);
}
