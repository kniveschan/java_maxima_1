package ru.maxima.hibernate.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.maxima.hibernate.models.OldCourse;
import ru.maxima.hibernate.models.OldLesson;
import ru.maxima.hibernate.models.OldStudent;

import java.util.ArrayList;

/**
 * 07.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainManyToOne {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        OldStudent student = OldStudent.builder()
                .firstName("Марсель")
                .lastName("Сидиков")
                .courses(new ArrayList<>())
                .build();

        session.save(student);

        // создал курс
        OldCourse java = OldCourse.builder()
                .title("Java")
                .build();

        // сохранил курс
        session.save(java);

        // создал курс
        OldCourse sql = OldCourse.builder()
                .title("SQL")
                .build();

        // сохранил курс
        session.save(sql);

        // создал урок, указал у урока уже сохраненный курс
        OldLesson javaCore = OldLesson.builder()
                .course(java)
                .name("Java core")
                .build();

        // создал урок, указал у урока уже сохраненный курс
        OldLesson spring = OldLesson.builder()
                .course(java)
                .name("Spring")
                .build();

        // создал урок, указал у урока уже сохраненный курс
        OldLesson selects = OldLesson.builder()
                .course(sql)
                .name("Select")
                .build();

        // создал урок, указал у урока уже сохраненный курс
        OldLesson inserts = OldLesson.builder()
                .course(sql)
                .name("Insert")
                .build();

        // сохранил уроки
        session.save(javaCore);
        session.save(spring);
        session.save(selects);
        session.save(inserts);

        session.close();
        sessionFactory.close();
    }
}
