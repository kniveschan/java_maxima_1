package ru.maxima.hibernate.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.maxima.hibernate.models.OldCourse;
import ru.maxima.hibernate.models.OldStudent;

import java.util.ArrayList;

/**
 * 07.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainManyToMany {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();
        // начинаем транзакцию
        session.beginTransaction();
        OldStudent student1 = OldStudent.builder()
                .firstName("Марсель")
                .lastName("Сидиков")
                .courses(new ArrayList<>())
                .build();

        OldStudent student2 = OldStudent.builder()
                .firstName("Айрат")
                .lastName("Мухутдинов")
                .courses(new ArrayList<>())
                .build();

        session.save(student1);
        session.save(student2);

        OldCourse java = OldCourse.builder()
                .title("Java")
                .build();

        OldCourse sql = OldCourse.builder()
                .title("SQL")
                .build();

        session.save(java);
        session.save(sql);

        student1.getCourses().add(sql);
        student2.getCourses().add(java);

        session.save(student1);
        session.save(student2);

        session.getTransaction().commit();

        session.close();
        sessionFactory.close();
    }
}
