package ru.maxima.hibernate.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.maxima.hibernate.models.OldCourse;
import ru.maxima.hibernate.models.OldLesson;
import ru.maxima.hibernate.models.OldStudent;

import java.util.ArrayList;

/**
 * 07.10.2021
 * 43. Hibernate
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainInitializeData {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        // создали курсы
        OldCourse java = OldCourse.builder()
                .title("Java")
                .build();

        OldCourse sql = OldCourse.builder()
                .title("SQL")
                .build();

        // сохранили курсы
        session.save(java);
        session.save(sql);

        // создал урок, указал у урока уже сохраненный курс
        OldLesson javaCore = OldLesson.builder()
                .course(java)
                .name("Java core")
                .build();

        // создал урок, указал у урока уже сохраненный курс
        OldLesson spring = OldLesson.builder()
                .course(java)
                .name("Spring")
                .build();

        // создал урок, указал у урока уже сохраненный курс
        OldLesson selects = OldLesson.builder()
                .course(sql)
                .name("Select")
                .build();

        // создал урок, указал у урока уже сохраненный курс
        OldLesson inserts = OldLesson.builder()
                .course(sql)
                .name("Insert")
                .build();

        // сохранили уроки
        session.save(javaCore);
        session.save(spring);
        session.save(selects);
        session.save(inserts);

        // создали студентов
        OldStudent student1 = OldStudent.builder()
                .firstName("Марсель")
                .lastName("Сидиков")
                .courses(new ArrayList<>())
                .build();

        OldStudent student2 = OldStudent.builder()
                .firstName("Айрат")
                .lastName("Мухутдинов")
                .courses(new ArrayList<>())
                .build();

        // сохранили студентов
        session.save(student1);
        session.save(student2);

        // добавили студентам курсы
        student1.getCourses().add(sql);
        student1.getCourses().add(java);
        student2.getCourses().add(sql);
        student2.getCourses().add(java);
        // обновили студентов
        session.save(student1);
        session.save(student2);

        session.getTransaction().commit();
        session.close();

    }
}
