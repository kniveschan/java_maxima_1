package ru.maxima.lambdas.example2;

/**
 * 29.06.2021
 * 19. Anons and Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface FilterFunction {
    boolean test(int element);
}
