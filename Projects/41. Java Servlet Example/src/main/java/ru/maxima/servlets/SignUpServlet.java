package ru.maxima.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.maxima.config.ApplicationConfig;
import ru.maxima.services.UsersService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/signUp")
public class SignUpServlet extends HttpServlet {
    private UsersService usersService;

    @Override
    public void init() throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.usersService = context.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().println("<h1>Sign Up</h1>\n" +
                "<p>Please enter your data</p>\n" +
                "<br>\n" +
                "<form method=\"post\">\n" +
                "\t<label for=\"email\">Enter email here:</label>\n" +
                "\t<input id=\"email\" type=\"text\" name=\"email\" placeholder=\"Your email\">\n" +
                "\t<label for=\"password\">Enter password here:</label>\n" +
                "\t<input id=\"password\" type=\"text\" name=\"password\" placeholder=\"Your password\">\n" +
                "\t<input type=\"submit\" name=\"\" value=\"Sign Up\">\n" +
                "</form>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        usersService.signUp(email, password);
        // отправляем браузеру Header location=/users, и браузер сам перейдет по этому урлу
        response.sendRedirect("/users");
    }
}
