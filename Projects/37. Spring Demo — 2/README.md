# Spring - Часть 2

* `@Autowired` - аннотация для автосвязывания. Spring сам найден подходящий по типу бин и вставит его в место связывания.

* `@Qualifier` - аннотация, позволяющая уточнить бин, который следует использовать. Указывается id-конкретного бина.

## Новый тип конфигурации системы

* В `context.xml` можем оставить только объявления бинов, связывание бинов делаем с помощью аннотаций `@Autowired` и `@Qualifier`.

```xml
<context:annotation-config>true</context:annotation-config>
```

* Свойства наших бинов вынесли в отдельный `application.properties`-файл, т.е. разделили описания свойств и объявление бинов.

```xml
<context:property-placeholder location="classpath:application.properties"/>
```
 