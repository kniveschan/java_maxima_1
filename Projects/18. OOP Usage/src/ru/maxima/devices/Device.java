package ru.maxima.devices;

/**
 * 27.06.2021
 * 18. OOP Usage
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// устройство, которое может выводить или считывать информацию
public interface Device {
    // выводит основную информацию о том, как работает это устройство
    String getInformation();
}
